const express = require('express');
const router = express.Router();

const mediaController = require('./handler');
const uploader = require('../middlewares/uploader');

router.get('/', mediaController.get);
router.post('/', uploader, mediaController.create);
router.delete('/:id', mediaController.destroy);

module.exports = router;
