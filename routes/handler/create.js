const { Media } = require('../../models');
const Imagekit = require('imagekit');
const isBase64 = require('is-base64');

const imagekitInstance = new Imagekit({
    publicKey: process.env.IMAGEKIT_PUBLIC_KEY,
    privateKey: process.env.IMAGEKIT_PRIVATE_KEY,
    urlEndpoint: `https://ik.imagekit.io/${process.env.IMAGEKIT_ID}`
});

module.exports = async (req, res) => {
    const image = req.body.image;
    if (!isBase64(image, { mimeRequired: true })) {
        return res.status(400).json({ status: 'error', message: 'invalid base64' });
    }

    imagekitInstance
        .upload({
            file: image,
            // file: req.file.buffer.toString("base64"),
            fileName: `img-${Date.now()}`,
            folder: 'online-course'
        })
        .then(image => {
            return Media.create({ image: image.url });
        })
        .then(data => {
            return res.status(201).json({ status: 'success', data });
        })
        .catch(err => {
            return res.status(422).json({ status: 'error', message: err });
        })
};