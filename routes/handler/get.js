const { Media } = require('../../models');

module.exports = async (req, res) => {
    const media = await Media.findAll();
    res.json({ status: 'success', message: media })
};