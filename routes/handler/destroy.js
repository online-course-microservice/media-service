const { Media } = require('../../models');

module.exports = async(req,res)=>{
    const id = req.params.id;

    const media = await Media.findByPk(id);
  
    if (!media) {
      return res.status(404).json({ status: 'error', message: 'media not found' });
    }
    
    await media.destroy();
  
    res.json({ status: 'success', message: 'image deleted' });
}