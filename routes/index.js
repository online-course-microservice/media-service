const express = require('express');
const router = express.Router();

const mediaRouter = require('./media');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.status(200).json({ status: 'success', message: 'welcome to service media' });
});

router.use('/media', mediaRouter);

module.exports = router;
